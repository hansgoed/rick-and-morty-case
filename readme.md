# Rick and Morty case
Symfony project with a Client for the [Rick and Morty API](https://rickandmortyapi.com/)

## Getting started
### Method 1: Symfony webserver
This project can be run with the Symfony built-in webserver. 
If you don't have the Symfony CLI installed, [install it first](https://symfony.com/download).

* Open a command window at the root of the project
* Run `composer install`
* Run `symfony serve`

A server will be started, and the command will show you a link to the website.

### Method 2: Docker
This project ships with a docker-compose file. 
If you don't have docker installed, [install it first](https://docs.docker.com/install/).

* Open a command window at the root of the project
* Run `docker-compose up`

On first run, all containers will be downloaded and initialized before the application is ready.

The website will become available on [http://rickandmorty.localhost:8080](http://rickandmorty.localhost:8080)

## Features
* Feature complete API client for the [Rick and Morty REST API](https://rickandmortyapi.com/) with PhpUnit tests.
* Feature complete website:
    * Character overview
    * Character details
    * Episode overview
    * Episode details
    * Location overview
    * Location details

### Interesting bits I'd hate for you to miss
* Symfony 5
* Serializer
    * Custom normalizers
* OptionsResolver
* ParamConverter
* HttpClient

## Issues
There are still a lot of issues I'd like to address, but it's time I'd deliver something. 

I've added a few Gitlab issues that I will address later.

There are also some issues for features I will add later too. 