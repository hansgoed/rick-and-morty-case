<?php

namespace App\Tests\Controller;

use App\Controller\EpisodeController;
use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\Episode;
use App\RickAndMortyApi\Response\EpisodeCollection;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Link\LocationLink;
use App\RickAndMortyApi\Response\Metadata;
use DateTime;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Tests for {@see EpisodeController}.
 *
 * @package App\Tests\Controller
 */
class EpisodeControllerTest extends TestCase
{
    /**
     * @var MockObject|Environment
     */
    private $twigMock;

    /**
     * @var EpisodeController
     */
    private $controller;

    /**
     * @var MockObject|Client
     */
    private $clientMock;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        $this->twigMock = $this->getMockBuilder(Environment::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->controller = new EpisodeController($this->twigMock);
    }

    /**
     * Test that {@see EpisodeController::showEpisodes} fetches all episodes correctly & uses these to make a response.
     */
    public function testShowEpisodes(): void
    {
        $episodes = [
            $this->createEpisode(),
            $this->createEpisode(),
            $this->createEpisode(),
            $this->createEpisode(),
        ];

        $this->clientMock
            ->method('getAllEpisodes')
            ->willReturn($episodes);

        $this->twigMock->expects($this->once())
            ->method('render')
            ->with($this->anything(), [
                'episodes' => $episodes
            ])
            ->willReturn('asdf');

        $expectedResponse = new Response('asdf');

        $response = $this->controller->showEpisodes($this->clientMock);

        $this->assertEquals($expectedResponse, $response);
    }

    /**
     * Test that {@see EpisodeController::showEpisode} takes the episode data and forwards it to the renderer and
     * returns the output in a {@see Response}.
     *
     * @throws Exception
     */
    public function testShowEpisode()
    {
        $characterLinks = [
            new CharacterLink('https://rickandmortyapi.com/api/character/1'),
            new CharacterLink('https://rickandmortyapi.com/api/character/2'),
        ];

        $episode = $this->createEpisode($characterLinks);

        $characters = [
            $this->createCharacter(),
            $this->createCharacter(),
        ];

        $this->clientMock->expects($this->once())
            ->method('getCharactersByLinks')
            ->with($characterLinks)
            ->willReturn($characters);

        $this->twigMock->expects($this->once())
            ->method('render')
            ->with($this->anything(), [
                'episode' => $episode,
                'characters' => $characters,
            ])
            ->willReturn('foo');

        $expectedResponse = new Response('foo');

        $response = $this->controller->showEpisode($episode, $this->clientMock);

        $this->assertEquals($expectedResponse, $response);
    }

    /**
     * Create a very fake character.
     *
     * @return Character
     */
    private function createCharacter(): Character
    {
        return new Character(
            1337,
            'Unit test character',
            'Not real',
            'Human',
            '',
            'Unknown',
            new LocationLink('', ''),
            new LocationLink('', ''),
            '',
            [],
            '',
            new DateTime()
        );
    }

    /**
     * Create a very fake episode.
     *
     * @param array $characterLinks
     *
     * @return Episode
     */
    private function createEpisode(array $characterLinks = []): Episode
    {
        return new Episode(
            1,
            '',
            new DateTime(),
            '',
            $characterLinks,
            '',
            new DateTime()
        );
    }
}
