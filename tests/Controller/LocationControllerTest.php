<?php

namespace App\Tests\Controller;

use App\Controller\LocationController;
use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Link\LocationLink;
use App\RickAndMortyApi\Response\Location;
use App\RickAndMortyApi\Response\LocationCollection;
use App\RickAndMortyApi\Response\Metadata;
use DateTime;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Tests for {@see LocationController}.
 *
 * @package App\Tests\Controller
 */
class LocationControllerTest extends TestCase
{
    /**
     * @var MockObject|Environment
     */
    private $twigMock;

    /**
     * @var LocationController
     */
    private $controller;

    /**
     * @var MockObject|Client
     */
    private $clientMock;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        $this->twigMock = $this->getMockBuilder(Environment::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->controller = new LocationController($this->twigMock);
    }

    /**
     * Test that {@see LocationController::showLocations} fetches all locations correctly & uses these to make a response.
     */
    public function testShowLocations(): void
    {
        $locations = [
            $this->createLocation(),
            $this->createLocation(),
            $this->createLocation(),
            $this->createLocation(),
        ];

        $this->clientMock
            ->method('getAllLocations')
            ->willReturn($locations);

        $this->twigMock->expects($this->once())
            ->method('render')
            ->with($this->anything(), [
                'locations' => $locations
            ])
            ->willReturn('asdf');

        $expectedResponse = new Response('asdf');

        $response = $this->controller->showLocations($this->clientMock);

        $this->assertEquals($expectedResponse, $response);
    }

    /**
     * Test that {@see LocationController::showLocation} takes the location data and forwards it to the renderer and
     * returns the output in a {@see Response}.
     *
     * @throws Exception
     */
    public function testShowLocation()
    {
        $characterLinks = [
            new CharacterLink('https://rickandmortyapi.com/api/character/1'),
            new CharacterLink('https://rickandmortyapi.com/api/character/2')
        ];

        $location = new Location(
            1,
            '',
            '',
            '',
            $characterLinks,
            '',
            new DateTime()
        );

        $character1 = $this->createCharacter();
        $character2 = $this->createCharacter();
        
        $this->clientMock
            ->method('getCharactersByLinks')
            ->willReturn([
                $character1,
                $character2,
            ]);

        $this->twigMock->expects($this->once())
            ->method('render')
            ->with($this->anything(), [
                'location' => $location,
                'residents' => [
                    $character1,
                    $character2
                ]
            ])
            ->willReturn('foo');

        $expectedResponse = new Response('foo');

        $response = $this->controller->showLocation($location, $this->clientMock);

        $this->assertEquals($expectedResponse, $response);
    }

    /**
     * Create a character to test with.
     */
    private function createCharacter(): Character
    {
        return new Character(
            1337,
            'Unit test character',
            'Not real',
            'Human',
            '',
            'Unknown',
            new LocationLink('', ''),
            new LocationLink('', ''),
            '',
            [],
            '',
            new DateTime()
        );
    }

    /**
     * Create a location to test with.
     */
    private function createLocation(int $id = 1337): Location
    {
        return new Location(
            $id,
            'Unit test location',
            'Not real',
            'Human',
            [],
            'Unknown',
            new DateTime()
        );
    }
}
