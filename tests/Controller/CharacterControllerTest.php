<?php

namespace App\Tests\Controller;

use App\Controller\CharacterController;
use App\Controller\EpisodeController;
use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\CharacterCollection;
use App\RickAndMortyApi\Response\Episode;
use App\RickAndMortyApi\Response\Link\EpisodeLink;
use App\RickAndMortyApi\Response\Link\LocationLink;
use App\RickAndMortyApi\Response\Location;
use App\RickAndMortyApi\Response\Metadata;
use DateTime;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Tests for {@see CharacterController}.
 *
 * @package App\Tests\Controller
 */
class CharacterControllerTest extends TestCase
{
    /**
     * @var MockObject|Environment
     */
    private $twigMock;

    /**
     * @var CharacterController
     */
    private $controller;

    /**
     * @var MockObject|Client
     */
    private $clientMock;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        $this->twigMock = $this->getMockBuilder(Environment::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->controller = new CharacterController($this->twigMock);
    }

    /**
     * Test that {@see EpisodeController::showEpisodes} fetches all episodes correctly & uses these to make a response.
     */
    public function testShowCharacters(): void
    {
        $characters = [
            $this->createCharacter(),
            $this->createCharacter(),
            $this->createCharacter(),
            $this->createCharacter(),
        ];

        $this->clientMock
            ->method('getAllCharacters')
            ->willReturn($characters);

        $this->twigMock->expects($this->once())
            ->method('render')
            ->with($this->anything(), [
                'characters' => $characters
            ])
            ->willReturn('asdf');

        $expectedResponse = new Response('asdf');

        $response = $this->controller->showCharacters($this->clientMock);

        $this->assertEquals($expectedResponse, $response);
    }

    /**
     * Test that {@see EpisodeController::showEpisode} takes the episode data and forwards it to the renderer and
     * returns the output in a {@see Response}.
     *
     * @throws Exception
     */
    public function testShowCharacter()
    {
        $locationLink1 = new LocationLink('', 'https://rickandmortyapi.com/api/location/3');
        $locationLink2 = new LocationLink('', 'https://rickandmortyapi.com/api/location/4');

        $character = new Character(
            1,
            '',
            '',
            '',
            '',
            '',
            $locationLink1,
            $locationLink2,
            '',
            [
                new EpisodeLink("https://rickandmortyapi.com/api/episode/1"),
                new EpisodeLink("https://rickandmortyapi.com/api/episode/2"),
            ],
            '',
            new DateTime()
        );

        $episode1 = $this->createEpisode();
        $episode2 = $this->createEpisode();

        $this->clientMock->method('getEpisodesById')
            ->with([
                1,
                2
            ])
            ->willReturn([
                $episode1,
                $episode2
            ]);

        $location1 = $this->createLocation();
        $location2 = $this->createLocation();

        $this->clientMock->method('getLocationByLink')
            ->withConsecutive(
                [$locationLink1],
                [$locationLink2]
            )
            ->willReturnOnConsecutiveCalls(
                $location1,
                $location2
            );

        $this->twigMock->expects($this->once())
            ->method('render')
            ->with($this->anything(), [
                'character' => $character,
                'episodes' => [$episode1, $episode2],
                'origin' => $location1,
                'location' => $location2,
            ])
            ->willReturn('foo');

        $expectedResponse = new Response('foo');

        $response = $this->controller->showCharacter(
            $character,
            $this->clientMock
        );

        $this->assertEquals($expectedResponse, $response);
    }

    /**
     * Create a very fake epsiode.
     *
     * @return Episode
     */
    private function createEpisode()
    {
        return new Episode(
            1337,
            'Unit test episode',
            new DateTime(),
            'S13E37',
            [],
            '',
            new DateTime()
        );
    }

    /**
     * Create a very fake location.
     *
     * @return Location
     */
    private function createLocation(): Location
    {
        return new Location(
            1337,
            'Unit test location',
            'Not real',
            'Human',
            [],
            'Unknown',
            new DateTime()
        );
    }

    /**
     * Create a very fake character.
     */
    private function createCharacter(): Character
    {
        return new Character(
            1337,
            'Unit test character',
            'Not real',
            'Human',
            '',
            'Unknown',
            new LocationLink('', ''),
            new LocationLink('', ''),
            '',
            [],
            '',
            new DateTime()
        );
    }
}
