<?php

namespace App\Tests\RickAndMortyApi\Response;

use App\RickAndMortyApi\Normalizer\EpisodeLinkDenormalizer;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\Link\EpisodeLink;
use App\RickAndMortyApi\Response\Link\LocationLink;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Tests for {@see Character}.
 */
class CharacterTest extends TestCase
{
    /**
     * Test that deserialization can be executed & the mappings are correct.
     */
    public function testDeserialization()
    {
        $phpDocExtractor = new PhpDocExtractor();
        $normalizer = new ObjectNormalizer(null, null, null, $phpDocExtractor);

        $serializer = new Serializer([
            new DateTimeNormalizer(),
            new EpisodeLinkDenormalizer(),
            $normalizer,
            new ArrayDenormalizer(),
        ],
        [
            new JsonEncoder(),
        ]);

        // Use the character response from the API docs to validate deserialization.
        $characterJson = file_get_contents('./tests/RickAndMortyApi/character.json');

        /** @var Character $character */
        $character = $serializer->deserialize($characterJson, Character::class, 'json');

        $this->assertEquals(2, $character->getId());
        $this->assertEquals("Morty Smith", $character->getName());
        $this->assertEquals("Alive", $character->getStatus());
        $this->assertEquals("Human", $character->getSpecies());
        $this->assertEquals("", $character->getType());
        $this->assertEquals("Male", $character->getGender());

        $this->assertEquals(
            new LocationLink("Earth", "https://rickandmortyapi.com/api/location/1"),
            $character->getOrigin()
        );

        $this->assertEquals(
            new LocationLink("Earth", "https://rickandmortyapi.com/api/location/20"),
            $character->getLocation()
        );

        $this->assertEquals("https://rickandmortyapi.com/api/character/avatar/2.jpeg", $character->getImage());

        $this->assertEquals(
            [
                new EpisodeLink("https://rickandmortyapi.com/api/episode/1"),
                new EpisodeLink("https://rickandmortyapi.com/api/episode/2")
            ],
            $character->getEpisode()
        );
        $this->assertEquals("https://rickandmortyapi.com/api/character/2", $character->getUrl());

        $expectedCreated = new \DateTime();
        $expectedCreated->setDate(2017, 11, 04);
        $expectedCreated->setTime(19, 50, 21, 651000);
        $expectedCreated->setTimezone(new \DateTimeZone("+00:00"));
        $this->assertEquals($expectedCreated, $character->getCreated());
    }
}