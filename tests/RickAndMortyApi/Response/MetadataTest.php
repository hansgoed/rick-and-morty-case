<?php

namespace App\Tests\RickAndMortyApi\Response;

use App\RickAndMortyApi\Response\Metadata;
use LogicException;
use PHPUnit\Framework\TestCase;

/**
 * Tests for {@see Metadata}
 *
 * @package App\Tests\RickAndMortyApi\Response
 */
class MetadataTest extends TestCase
{
    /**
     * Test that attributes passed to the constructor are mapped to the right properties.
     */
    public function testConstruct(): void
    {
        $metadata = new Metadata(
            21,
            55,
            '',
            ''
        );

        $this->assertEquals(21, $metadata->getCount());
        $this->assertEquals(55, $metadata->getPages());
    }

    /**
     * Test that {@see Metadata::getNextPageNumber} successfully extracts a page number from several different
     * possible URLs and returns them.
     *
     * @param string $nextUrl
     * @param int $expectedPageNumber
     *
     * @dataProvider urlsToParseProvider
     */
    public function testGetNextPageNumber(string $nextUrl, int $expectedPageNumber)
    {
        $metadata = new Metadata(21, 55, $nextUrl, '');

        $this->assertEquals($expectedPageNumber, $metadata->getNextPageNumber());
    }

    /**
     * Test that {@see Metadata::getPreviousPageNumber} successfully extracts a page number from several different
     * possible URLs and returns them.
     *
     * @param string $previousUrl
     * @param int $expectedPageNumber
     *
     * @dataProvider urlsToParseProvider
     */
    public function testGetPreviousPageNumber(string $previousUrl, int $expectedPageNumber)
    {
        $metadata = new Metadata(21, 55, '', $previousUrl);

        $this->assertEquals($expectedPageNumber, $metadata->getPreviousPageNumber());
    }

    /**
     * Data provider that provides valid "next" URLs.
     *
     * @return array
     */
    public function urlsToParseProvider()
    {
        return [
            'character' => [
                'https://rickandmortyapi.com/api/character?page=32',
                32
            ],
            'character-with-trailing-slash' => [
                'https://rickandmortyapi.com/api/character/?page=255',
                255
            ],
            'location' => [
                'https://rickandmortyapi.com/api/location?page=33',
                33
            ],
            'location-with-trailing-slash' => [
                'https://rickandmortyapi.com/api/location/?page=256',
                256
            ],
            'episode' => [
                'https://rickandmortyapi.com/api/episode?page=34',
                34
            ],
            'episode-with-trailing-slash' => [
                'https://rickandmortyapi.com/api/episode/?page=257',
                257
            ],
            'episode-with-filters' => [
                'https://rickandmortyapi.com/api/episode/?page=258&episode=S02E15',
                258
            ],
            'episode-with-filters-page-is-last-item-in-querystring' => [
                'https://rickandmortyapi.com/api/episode/?episode=S02E15&page=259',
                259
            ],
        ];
    }

    /**
     * Test that {@see Metadata::getNextPageNumber} throws an exception when the URL is not in the expected format.
     *
     * @param string $nextUrl
     * @param string $expectedExceptionMessage
     *
     * @dataProvider invalidUrlsToParseProvider
     */
    public function testGetNextPageNumberThrowsExceptionWhenParsingIsNotPossible(
        string $nextUrl,
        string $expectedExceptionMessage
    ): void
    {
        $metadata = new Metadata(21, 55, $nextUrl, '');

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage($expectedExceptionMessage);

        $metadata->getNextPageNumber();
    }

    /**
     * Data provider that provides URLs that do not have a (valid) page number in them, and thus can't be parsed.
     *
     * @return array
     */
    public function invalidUrlsToParseProvider(): iterable
    {
        return [
            'missing-query-string' => [
                'https://rickandmortyapi.com/api/character/',
                'Page link "https://rickandmortyapi.com/api/character/" doesn\'t have a "page" item in the querystring.',
            ],
            'missing-query-string-but-questionmark-is-there' => [
                'https://rickandmortyapi.com/api/location?',
                'Page link "https://rickandmortyapi.com/api/location?" doesn\'t have a "page" item in the querystring.',
            ],
            'page-number-is-not-a-number' => [
                'https://rickandmortyapi.com/api/location/?page=twohundredfiftysix',
                'Page link "https://rickandmortyapi.com/api/location/?page=twohundredfiftysix" doesn\'t have a valid "page" item in the querystring.',
            ],
        ];
    }
}
