<?php

namespace App\Tests\RickAndMortyApi\Response;

use App\RickAndMortyApi\Normalizer\CharacterLinkDenormalizer;
use App\RickAndMortyApi\Response\Episode;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Tests for {@see Episode}.
 *
 * @package App\Tests\RickAndMortyApi\Response
 */
class EpisodeTest extends TestCase
{
    /**
     * Test that deserialization can be executed & the mappings are correct.
     */
    public function testDeserialization()
    {
        $phpDocExtractor = new PhpDocExtractor();
        $normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter(), null, $phpDocExtractor);

        $serializer = new Serializer([
            new CharacterLinkDenormalizer(),
            new DateTimeNormalizer(),
            $normalizer,
            new ArrayDenormalizer(),
        ],
        [
            new JsonEncoder(),
        ]);

        // Use the episode response from the API docs to validate deserialization.
        $episodeJson = file_get_contents('./tests/RickAndMortyApi/episode.json');

        /** @var Episode $episode */
        $episode = $serializer->deserialize($episodeJson, Episode::class, 'json');

        $this->assertEquals(28, $episode->getId());
        $this->assertEquals("The Ricklantis Mixup", $episode->getName());

        $expectedAirDate = new \DateTime("2017-09-10");
        $this->assertEquals($expectedAirDate, $episode->getAirDate());

        $this->assertEquals("S03E07", $episode->getEpisode());
        $this->assertEquals([
            new CharacterLink("https://rickandmortyapi.com/api/character/1"),
            new CharacterLink("https://rickandmortyapi.com/api/character/2"),
        ], $episode->getCharacters());

        $this->assertEquals("https://rickandmortyapi.com/api/episode/28", $episode->getUrl());

        $expectedCreated = new \DateTime();
        $expectedCreated->setDate(2017, 11, 10);
        $expectedCreated->setTime(13, 56, 36, 618000);
        $expectedCreated->setTimezone(new \DateTimeZone("+00:00"));

        $this->assertEquals($expectedCreated, $episode->getCreated());
    }
}
