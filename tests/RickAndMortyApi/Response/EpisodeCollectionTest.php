<?php

namespace App\Tests\RickAndMortyApi\Response;

use App\RickAndMortyApi\Normalizer\CharacterLinkDenormalizer;
use App\RickAndMortyApi\Response\Episode;
use App\RickAndMortyApi\Response\EpisodeCollection;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Metadata;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Tests for {@see EpisodeCollection}.
 */
class EpisodeCollectionTest extends TestCase
{
    /**
     * Test that deserialization can be executed & the mappings are correct.
     */
    public function testDeserialize(): void
    {
        $phpDocExtractor = new PhpDocExtractor();
        $normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter(), null, $phpDocExtractor);

        $serializer = new Serializer([
            new DateTimeNormalizer(),
            new CharacterLinkDenormalizer(),
            $normalizer,
            new ArrayDenormalizer(),
        ],
        [
            new JsonEncoder(),
        ]);

        // Use the all episodes response from the API docs to validate deserialization.
        $episodeCollectionJson = file_get_contents('./tests/RickAndMortyApi/episodeCollection.json');

        /** @var EpisodeCollection $episodeCollection */
        $episodeCollection = $serializer->deserialize($episodeCollectionJson, EpisodeCollection::class, 'json');

        $expectedMetadata = new Metadata(
            31,
            2,
            "https://rickandmortyapi.com/api/episode?page=2",
            ""
        );
        $this->assertEquals($expectedMetadata, $episodeCollection->getInfo());

        $expectedResults = [
            new Episode(
                1,
                "Pilot",
                new \DateTime("2013-12-02 00:00:00"),
                "S01E01",
                [
                    new CharacterLink('https://rickandmortyapi.com/api/character/1'),
                    new CharacterLink('https://rickandmortyapi.com/api/character/2')
                ],
                "https://rickandmortyapi.com/api/episode/1",
                new \DateTime("2017-11-10T12:56:33.798Z")
            ),
            new Episode(
                28,
                "The Ricklantis Mixup",
                new \DateTime("2017-09-10 00:00:00"),
                "S03E07",
                [
                    new CharacterLink('https://rickandmortyapi.com/api/character/1'),
                    new CharacterLink('https://rickandmortyapi.com/api/character/2')
                ],
                "https://rickandmortyapi.com/api/episode/28",
                new \DateTime("2017-11-10T12:56:36.618Z")
            ),
        ];

        $this->assertEquals($expectedResults, $episodeCollection->getResults());
    }
}
