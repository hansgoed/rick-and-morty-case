<?php

namespace App\Tests\RickAndMortyApi\Response;

use App\RickAndMortyApi\Normalizer\CharacterLinkDenormalizer;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Location;
use App\RickAndMortyApi\Response\LocationCollection;
use App\RickAndMortyApi\Response\Metadata;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Tests for {@see LocationCollection}.
 */
class LocationCollectionTest extends TestCase
{
    /**
     * Test that deserialization can be executed & the mappings are correct.
     */
    public function testDeserialize(): void
    {
        $phpDocExtractor = new PhpDocExtractor();
        $normalizer = new ObjectNormalizer(null, null, null, $phpDocExtractor);

        $serializer = new Serializer([
            new DateTimeNormalizer(),
            new CharacterLinkDenormalizer(),
            $normalizer,
            new ArrayDenormalizer(),
        ],
        [
            new JsonEncoder(),
        ]);

        // Use the all locations response from the API docs to validate deserialization.
        $locationCollectionJson = file_get_contents('./tests/RickAndMortyApi/locationCollection.json');

        /** @var LocationCollection $locationCollection */
        $locationCollection = $serializer->deserialize($locationCollectionJson, LocationCollection::class, 'json');

        $expectedMetadata = new Metadata(
            67,
            4,
            "https://rickandmortyapi.com/api/location?page=2",
            ""
        );
        $this->assertEquals($expectedMetadata, $locationCollection->getInfo());

        $expectedResults = [
            new Location(
                1,
                "Earth",
                "Planet",
                "Dimension C-137",
                [
                    new CharacterLink('https://rickandmortyapi.com/api/character/1'),
                    new CharacterLink('https://rickandmortyapi.com/api/character/2')
                ],
                "https://rickandmortyapi.com/api/location/1",
                new \DateTime("2017-11-10T12:42:04.162Z")
            ),
            new Location(
                3,
                "Citadel of Ricks",
                "Space station",
                "unknown",
                [
                    new CharacterLink('https://rickandmortyapi.com/api/character/8'),
                    new CharacterLink('https://rickandmortyapi.com/api/character/14')
                ],
                "https://rickandmortyapi.com/api/location/3",
                new \DateTime("2017-11-10T13:08:13.191Z")
            ),
        ];

        $this->assertEquals($expectedResults, $locationCollection->getResults());
    }
}
