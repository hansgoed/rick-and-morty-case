<?php

namespace App\Tests\RickAndMortyApi\Response;

use App\RickAndMortyApi\Normalizer\EpisodeLinkDenormalizer;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\CharacterCollection;
use App\RickAndMortyApi\Response\Link\EpisodeLink;
use App\RickAndMortyApi\Response\Link\LocationLink;
use App\RickAndMortyApi\Response\Metadata;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Tests for {@see CharacterCollection}.
 */
class CharacterCollectionTest extends TestCase
{
    public function testDeserialize(): void
    {
        $phpDocExtractor = new PhpDocExtractor();
        $normalizer = new ObjectNormalizer(null, null, null, $phpDocExtractor);

        $serializer = new Serializer([
            new DateTimeNormalizer(),
            new EpisodeLinkDenormalizer(),
            $normalizer,
            new ArrayDenormalizer(),
        ],
        [
            new JsonEncoder(),
        ]);

        // Use the all characters response from the API docs to validate deserialization.
        $characterCollectionJson = file_get_contents('./tests/RickAndMortyApi/characterCollection.json');

        /** @var CharacterCollection $characterCollection */
        $characterCollection = $serializer->deserialize($characterCollectionJson, CharacterCollection::class, 'json');

        $expectedMetadata = new Metadata(
            394,
            20,
            "https://rickandmortyapi.com/api/character/?page=2",
            ""
        );
        $this->assertEquals($expectedMetadata, $characterCollection->getInfo());

        $expectedResults = [
            new Character(
                1,
                "Rick Sanchez",
                "Alive",
                "Human",
                "",
                "Male",
                new LocationLink("Earth", "https://rickandmortyapi.com/api/location/1"),
                new LocationLink("Earth", "https://rickandmortyapi.com/api/location/20"),
                "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
                [
                    new EpisodeLink("https://rickandmortyapi.com/api/episode/1"),
                    new EpisodeLink("https://rickandmortyapi.com/api/episode/2"),
                ],
                "https://rickandmortyapi.com/api/character/1",
                new \DateTime("2017-11-04T18:48:46.250Z")
            ),
            new Character(
                2,
                "Morty Smith",
                "Alive",
                "Human",
                "",
                "Male",
                new LocationLink("Earth", "https://rickandmortyapi.com/api/location/1"),
                new LocationLink("Earth", "https://rickandmortyapi.com/api/location/20"),
                "https://rickandmortyapi.com/api/character/avatar/2.jpeg",
                [
                    new EpisodeLink("https://rickandmortyapi.com/api/episode/1"),
                    new EpisodeLink("https://rickandmortyapi.com/api/episode/2"),
                ],
                "https://rickandmortyapi.com/api/character/2",
                new \DateTime("2017-11-04T18:50:21.651Z")
            ),
        ];

        $this->assertEquals($expectedResults, $characterCollection->getResults());
    }
}
