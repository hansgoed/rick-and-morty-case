<?php

namespace App\Tests\RickAndMortyApi\Response;

use App\RickAndMortyApi\Normalizer\CharacterLinkDenormalizer;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Location;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Tests for {@see Location}.
 *
 * @package App\Tests\RickAndMortyApi\Response
 */
class LocationTest extends TestCase
{
    /**
     * Test that deserialization can be executed & the mappings are correct.
     */
    public function testDeserialization()
    {
        $phpDocExtractor = new PhpDocExtractor();
        $normalizer = new ObjectNormalizer(null, null, null, $phpDocExtractor);

        $serializer = new Serializer([
            new CharacterLinkDenormalizer(),
            new DateTimeNormalizer(),
            $normalizer,
            new ArrayDenormalizer(),
        ],
        [
            new JsonEncoder(),
        ]);

        // Use the character response from the API docs to validate deserialization.
        $locationJson = file_get_contents('./tests/RickAndMortyApi/location.json');

        /** @var Location $location */
        $location = $serializer->deserialize($locationJson, Location::class, 'json');

        $this->assertEquals(3, $location->getId());
        $this->assertEquals("Citadel of Ricks", $location->getName());
        $this->assertEquals("Space station", $location->getType());
        $this->assertEquals("unknown", $location->getDimension());
        $this->assertEquals([
            new CharacterLink("https://rickandmortyapi.com/api/character/8"),
            new CharacterLink("https://rickandmortyapi.com/api/character/14"),
        ], $location->getResidents());
        $this->assertEquals("https://rickandmortyapi.com/api/location/3", $location->getUrl());

        $expectedCreatedAt = new \DateTime();
        $expectedCreatedAt->setDate(2017, 11, 10);
        $expectedCreatedAt->setTime(14,8,13,191000);
        $expectedCreatedAt->setTimezone(new \DateTimeZone("+00:00"));

        $this->assertEquals($expectedCreatedAt, $location->getCreated());
    }
}
