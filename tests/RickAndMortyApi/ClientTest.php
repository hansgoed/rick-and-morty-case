<?php

namespace App\Tests\RickAndMortyApi;

use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\CharacterCollection;
use App\RickAndMortyApi\Response\Episode;
use App\RickAndMortyApi\Response\EpisodeCollection;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Link\EpisodeLink;
use App\RickAndMortyApi\Response\Link\LocationLink;
use App\RickAndMortyApi\Response\Location;
use App\RickAndMortyApi\Response\LocationCollection;
use App\RickAndMortyApi\Response\Metadata;
use DateTime;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ClientTest extends TestCase
{
    /**
     * Client to be tested.
     *
     * @var Client
     */
    private $client;

    /**
     * @var MockObject|HttpClientInterface
     */
    private $httpClientMock;

    /**
     * @var MockObject|SerializerInterface
     */
    private $serializerMock;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        $this->httpClientMock = $this->getMockBuilder(HttpClientInterface::class)
            ->getMock();

        $this->serializerMock = $this->getMockBuilder(SerializerInterface::class)
            ->getMock();

        $this->client = new Client($this->httpClientMock, $this->serializerMock);
    }

    /**
     * Test that the correct request is performed & the JSON is deserialized when calling {@see Client::getCharacterById}.
     */
    public function testGetCharacterById(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/character/1337')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $character = new Character(
            1337,
            'Unit test character',
            'Not real',
            'Human',
            '',
            'Unknown',
            new LocationLink('', ''),
            new LocationLink('', ''),
            '',
            [],
            '',
            new DateTime()
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', Character::class, 'json')
            ->willReturn($character);

        $response = $this->client->getCharacterById(1337);
        $this->assertSame($character, $response);
    }

    /**
     * Test that the correct request is performed & the JSON is deserialized when calling {@see Client::getCharacterByLink}.
     */
    public function testGetCharacterByLink(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://link-to-character.com/api/character/ricky')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $character = new Character(
            1337,
            'Unit test character',
            'Not real',
            'Human',
            '',
            'Unknown',
            new LocationLink('', ''),
            new LocationLink('', ''),
            '',
            [],
            '',
            new DateTime()
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', Character::class, 'json')
            ->willReturn($character);

        $characterLink = new CharacterLink('https://link-to-character.com/api/character/ricky');

        $response = $this->client->getCharacterByLink($characterLink);
        $this->assertSame($character, $response);
    }

    /**
     * Test {@see Client::getCharacters} sends the correct request to the API & uses the data from the API to create a
     * {@see CharacterCollection} object.
     *
     * @param array $filters
     * @param array $expectedQueryOptions
     *
     * @dataProvider getCharactersFiltersProvider
     */
    public function testGetCharacters(array $filters, array $expectedQueryOptions)
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/character/', [
                'query' => $expectedQueryOptions
            ])
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $characterCollection = new CharacterCollection(
            new Metadata(1,2, "a", "b"),
            []
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', CharacterCollection::class, 'json')
            ->willReturn($characterCollection);

        $this->client->getCharacters($filters);
    }

    /**
     * Data provider that provides valid filters for the {@see Client::getCharacters} method.
     *
     * @return array
     */
    public function getCharactersFiltersProvider(): iterable
    {
        return [
            '#noFilter' => [
                [],
                [
                    'name' => null,
                    'status' => null,
                    'species' => null,
                    'type' => null,
                    'gender' => null,
                    'page' => null,
                ]
            ],
            'oneFilter' => [
                [
                    'name' => 'rick'
                ],
                [
                    'name' => 'rick',
                    'status' => null,
                    'species' => null,
                    'type' => null,
                    'gender' => null,
                    'page' => null,
                ]
            ],
            'allFilters' => [
                [
                    'name' => 'rick',
                    'status' => 'alive',
                    'species' => 'human',
                    'type' => '',
                    'gender' => 'male',
                ],
                [
                    'name' => 'rick',
                    'status' => 'alive',
                    'species' => 'human',
                    'type' => '',
                    'gender' => 'male',
                    'page' => null,
                ],
            ],
        ];
    }

    /**
     * Test that {@see Client::getCharacters} throws an exception when invalid options are given for the filters.
     *
     * @dataProvider getCharactersInvalidFiltersProvider
     *
     * @param array $filters
     * @param string $expectedExceptionType
     * @param string $expectedExceptionMessage
     */
    public function testGetCharactersThrowsExceptionWithInvalidFilters(
        array $filters,
        string $expectedExceptionType,
        string $expectedExceptionMessage
    ): void {
        $this->expectException($expectedExceptionType);
        $this->expectExceptionMessage($expectedExceptionMessage);

        $this->client->getCharacters($filters);
    }

    /**
     * Data provider that provides INvalid filters for the {@see Client::getCharacters} method.
     *
     * @return array
     */
    public function getCharactersInvalidFiltersProvider(): iterable
    {
        return [
            'Wrong name' => [
                [
                    'asdfdfs' => 'obvious bs',
                ],
                UndefinedOptionsException::class,
                'The option "asdfdfs" does not exist. Defined options are: "gender", "name", "species", "status", "type".',
            ],
            'Wrong value' => [
                [
                    'status' => 'undead',
                ],
                InvalidOptionsException::class,
                'The option "status" with value "undead" is invalid. Accepted values are: "alive", "dead", "unknown", null.',
            ],
            'Wrong value 2' => [
                [
                    'gender' => 'it\'s complicated',
                ],
                InvalidOptionsException::class,
                'The option "gender" with value "it\'s complicated" is invalid. Accepted values are: "female", "male", "genderless", "unknown", null.',
            ],
            'Multiple wrong values' => [
                [
                    'status' => 'undead',
                    'gender' => 'it\'s complicated',
                ],
                InvalidOptionsException::class,
                'The option "status" with value "undead" is invalid. Accepted values are: "alive", "dead", "unknown", null.',
            ]
        ];
    }

    /**
     * Test that the correct request is performed & the JSON is deserialized when calling {@see Client::getLocationById}.
     */
    public function testGetLocationById(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/location/1337')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $location = new Location(
            1337,
            'Unit test location',
            'Not real',
            'Human',
            [],
            'Unknown',
            new DateTime()
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', Location::class, 'json')
            ->willReturn($location);

        $response = $this->client->getLocationById(1337);
        $this->assertSame($location, $response);
    }

    /**
     * Test that the correct request is performed & the JSON is deserialized when calling {@see Client::getLocationByLink}.
     */
    public function testGetLocationByLink(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://link-to-character.com/api/location/earth')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $location = new Location(
            1337,
            'Unit test location',
            'Not real',
            'Human',
            [],
            'Unknown',
            new DateTime()
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', Location::class, 'json')
            ->willReturn($location);

        $locationLink = new LocationLink(
            'Earth',
            'https://link-to-character.com/api/location/earth'
        );

        $response = $this->client->getLocationByLink($locationLink);
        $this->assertSame($location, $response);
    }

    /**
     * Test {@see Client::getLocations} sends the correct request to the API & uses the data from the API to create a
     * {@see LocationCollection} object.
     *
     * @param array $filters
     * @param array $expectedQueryOptions
     *
     * @dataProvider getLocationsFiltersProvider
     */
    public function testGetLocations(array $filters, array $expectedQueryOptions)
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/location/', [
                'query' => $expectedQueryOptions
            ])
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $locationCollection = new LocationCollection(
            new Metadata(1,2, "a", "b"),
            []
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', LocationCollection::class, 'json')
            ->willReturn($locationCollection);

        $this->client->getLocations($filters);
    }

    /**
     * Data provider that provides valid filters for the {@see Client::getLocations} method.
     *
     * @return array
     */
    public function getLocationsFiltersProvider(): iterable
    {
        return [
            '#noFilter' => [
                [],
                [
                    'name' => null,
                    'type' => null,
                    'dimension' => null,
                    'page' => null,
                ]
            ],
            'oneFilter' => [
                [
                    'name' => 'earth'
                ],
                [
                    'name' => 'earth',
                    'type' => null,
                    'dimension' => null,
                    'page' => null,
                ]
            ],
            'allFilters' => [
                [
                    'name' => 'earth',
                    'type' => 'planet',
                    'dimension' => 'Dimension C-137',
                ],
                [
                    'name' => 'earth',
                    'type' => 'planet',
                    'dimension' => 'Dimension C-137',
                    'page' => null,
                ],
            ],
        ];
    }

    /**
     * Test that {@see Client::getLocations} throws an exception when invalid options are given for the filters.
     */
    public function testGetLocationsThrowsExceptionWithInvalidFilters(): void {
        $this->expectException(UndefinedOptionsException::class);
        $this->expectExceptionMessage('The option "asdfdfs" does not exist. Defined options are: "dimension", "name", "type".');

        $this->client->getLocations([
            'asdfdfs' => 'obvious bs',
        ]);
    }

    /**
     * Test that the correct request is performed & the JSON is deserialized when calling {@see Client::getEpisodeById}.
     */
    public function testGetEpisodeById(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/episode/1337')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $episode = new Episode(
            1337,
            'Unit test episode',
            new DateTime(),
            'S13E37',
            [],
            '',
            new DateTime()
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', Episode::class, 'json')
            ->willReturn($episode);

        $response = $this->client->getEpisodeById(1337);
        $this->assertSame($episode, $response);
    }

    /**
     * Test that the correct request is performed & the JSON is deserialized when calling {@see Client::getEpisodeByLink}.
     */
    public function testGetEpisodeByLink(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://link-to-character.com/api/episode/first')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $episode = new Episode(
            1337,
            'Unit test episode',
            new DateTime(),
            'S13E37',
            [],
            '',
            new DateTime()
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', Episode::class, 'json')
            ->willReturn($episode);

        $episodeLink = new EpisodeLink(
            'https://link-to-character.com/api/episode/first'
        );

        $response = $this->client->getEpisodeByLink($episodeLink);
        $this->assertSame($episode, $response);
    }

    /**
     * Test {@see Client::getEpisodes} sends the correct request to the API & uses the data from the API to create a
     * {@see EpisodeCollection} object.
     *
     * @param array $filters
     * @param array $expectedQueryOptions
     *
     * @dataProvider getEpisodesFiltersProvider
     */
    public function testGetEpisodes(array $filters, array $expectedQueryOptions)
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/episode/', [
                'query' => $expectedQueryOptions
            ])
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $episodeCollection = new EpisodeCollection(
            new Metadata(1,2, "a", "b"),
            []
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', EpisodeCollection::class, 'json')
            ->willReturn($episodeCollection);

        $this->client->getEpisodes($filters);
    }

    /**
     * Data provider that provides valid filters for the {@see Client::getEpisodes} method.
     *
     * @return array
     */
    public function getEpisodesFiltersProvider(): iterable
    {
        return [
            '#noFilter' => [
                [],
                [
                    'name' => null,
                    'episode' => null,
                    'page' => null,
                ]
            ],
            'oneFilter' => [
                [
                    'name' => 'first'
                ],
                [
                    'name' => 'first',
                    'episode' => null,
                    'page' => null,
                ]
            ],
            'allFilters' => [
                [
                    'name' => 'first',
                    'episode' => 'S01E01',
                ],
                [
                    'name' => 'first',
                    'episode' => 'S01E01',
                    'page' => null,
                ],
            ],
        ];
    }

    /**
     * Test that {@see Client::getEpisodes} throws an exception when invalid options are given for the filters.
     */
    public function testGetEpisodesThrowsExceptionWithInvalidFilters(): void {
        $this->expectException(UndefinedOptionsException::class);
        $this->expectExceptionMessage('The option "asdfdfs" does not exist. Defined options are: "episode", "name".');

        $this->client->getEpisodes([
            'asdfdfs' => 'obvious bs',
        ]);
    }

    /**
     * Test that {@see Client::getEpisodesById} executes the correct request, deserializes it's response & returns the
     * {@see Episode}s.
     */
    public function testGetEpisodesById(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/episode/1,2,3,55,87')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fakeJson')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $episode = new Episode(
            1337,
            'Unit test episode',
            new DateTime(),
            'S13E37',
            [],
            '',
            new DateTime()
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fakeJson', Episode::class . '[]', 'json')
            ->willReturn([
                $episode,
                $episode
            ]);

        $episodes = $this->client->getEpisodesById([1,2,3,55,87]);

        $this->assertSame([$episode, $episode], $episodes);
    }

    /**
     * Test that {@see Client::getEpisodesById} with a single id returns an array with a single {@see Episode}.
     *
     * Fixes MissingConstructorArgumentsException when only a single episode is linked to a character. The serializer
     * expects a JSON array but only receives a single item because the API endpoint is the same for a single episode.
     */
    public function testGetEpisodesByIdWithSingleEpisodeReturnsSingleEpisodeInArray(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/episode/1337')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $episode = new Episode(
            1337,
            'Unit test episode',
            new DateTime(),
            'S13E37',
            [],
            '',
            new DateTime()
        );

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', Episode::class, 'json')
            ->willReturn($episode);

        $response = $this->client->getEpisodesById([1337]);
        $this->assertSame([$episode], $response);
    }

    /**
     * Test that {@see Client::getCharactersByLinks} executes the correct request, deserializes it's response & returns
     * the {@see Character}s.
     */
    public function testGetCharactersByLinks(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/character/1,2,3,55,87')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fakeJson')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $character1 = $this->createCharacter();
        $character2 = $this->createCharacter();
        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fakeJson', Character::class . '[]', 'json')
            ->willReturn([
                $character1,
                $character2
            ]);

        $characters = $this->client->getCharactersByLinks([
            new CharacterLink('https://rickandmortyapi.com/api/character/1'),
            new CharacterLink('https://rickandmortyapi.com/api/character/2'),
            new CharacterLink('https://rickandmortyapi.com/api/character/3'),
            new CharacterLink('https://rickandmortyapi.com/api/character/55'),
            new CharacterLink('https://rickandmortyapi.com/api/character/87'),
        ]);

        $this->assertSame([$character1, $character2], $characters);
    }

    /**
     * Test that {@see Client::getCharactersByLinks()} with a single {@see CharacterLink} returns an array with a
     * single {@see Character}.
     *
     * Fixes MissingConstructorArgumentsException when only a single character is linked to a location. The serializer
     * expects a JSON array but only receives a single item because the API endpoint is the same for a single character.
     */
    public function testGetCharactersByLinksWithSingleCharacterReturnsSingleCharacterInArray(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/character/1337')
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $character = $this->createCharacter();

        $this->serializerMock->expects($this->once())
            ->method('deserialize')
            ->with('fake json', Character::class, 'json')
            ->willReturn($character);

        $response = $this->client->getCharactersByLinks([new CharacterLink('https://rickandmortyapi.com/api/character/1337')]);
        $this->assertSame([$character], $response);
    }

    /**
     * Test that {@see Client::getAllCharacters} deserializes multiple responses into one big array.
     *
     * @dataProvider getAllCharactersResponsesProvider
     */
    public function testGetAllCharacters(array $deserializedResponses, iterable $expectedResults)
    {
        $this->httpClientMock
            ->expects($this->exactly(count($deserializedResponses)))
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/character/', $this->anything())
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $this->serializerMock->method('deserialize')
            ->willReturnOnConsecutiveCalls(...$deserializedResponses);

        $allCharacters = $this->client->getAllCharacters();

        $this->assertEquals($expectedResults, $allCharacters);
    }

    /**
    * Data provider for different kinds of successful API responses that should (or not) result in a followup request.
    *
    * @return array
    */
    public function getAllCharactersResponsesProvider()
    {
        $character1 = $this->createCharacter(1);
        $character2 = $this->createCharacter(2);
        $character3 = $this->createCharacter(3);

        return [
            'one-page' => [
                [ // Deserialized values
                    new CharacterCollection(
                        new Metadata(1, 1, '', ''),
                        [
                            $character1,
                            $character2,
                        ]
                    ),
                ],
                [ // Expected results
                    $character1,
                    $character2,
                ]
            ],
            'three-pages' => [
                [ // Deserialized values
                    new CharacterCollection(
                        new Metadata(3, 3, 'https://rickandmortyapi.com/api/episode?page=2', ''),
                        [
                            $character1,
                        ]
                    ),
                    new CharacterCollection(
                        new Metadata(3, 3, 'https://rickandmortyapi.com/api/episode?page=3', 'https://rickandmortyapi.com/api/episode?page=1'),
                        [
                            $character2,
                        ]
                    ),
                    new CharacterCollection(
                        new Metadata(3, 3, '', 'https://rickandmortyapi.com/api/episode?page=2'),
                        [
                            $character3,
                        ]
                    ),
                ],
                [ // Expected results
                    $character1,
                    $character2,
                    $character3,
                ]
            ],
        ];
    }

    /**
     * Test that {@see Client::getAllEpisodes} deserializes multiple responses into one big array.
     *
     * @dataProvider getAllEpisodesResponsesProvider
     */
    public function testGetAllEpisodes(array $deserializedResponses, iterable $expectedResults)
    {
        $this->httpClientMock
            ->expects($this->exactly(count($deserializedResponses)))
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/episode/', $this->anything())
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $this->serializerMock->method('deserialize')
            ->willReturnOnConsecutiveCalls(...$deserializedResponses);

        $allEpisodes = $this->client->getAllEpisodes();

        $this->assertEquals($expectedResults, $allEpisodes);
    }

    /**
     * Data provider for different kinds of successful API responses that should (or not) result in a followup request.
     *
     * @return array
     */
    public function getAllEpisodesResponsesProvider()
    {
        $episode1 = $this->createEpisode(1);
        $episode2 = $this->createEpisode(2);
        $episode3 = $this->createEpisode(3);

        return [
            'one-page' => [
                [ // Deserialized values
                    new EpisodeCollection(
                        new Metadata(1, 1, '', ''),
                        [
                            $episode1,
                            $episode2,
                        ]
                    ),
                ],
                [ // Expected results
                    $episode1,
                    $episode2,
                ]
            ],
            'three-pages' => [
                [ // Deserialized values
                    new EpisodeCollection(
                        new Metadata(3, 3, 'https://rickandmortyapi.com/api/episode?page=2', ''),
                        [
                            $episode1,
                        ]
                    ),
                    new EpisodeCollection(
                        new Metadata(3, 3, 'https://rickandmortyapi.com/api/episode?page=3', 'https://rickandmortyapi.com/api/episode?page=1'),
                        [
                            $episode2,
                        ]
                    ),
                    new EpisodeCollection(
                        new Metadata(3, 3, '', 'https://rickandmortyapi.com/api/episode?page=2'),
                        [
                            $episode3,
                        ]
                    ),
                ],
                [ // Expected results
                    $episode1,
                    $episode2,
                    $episode3,
                ]
            ],
        ];
    }

    /**
     * Test that {@see Client::getAllLocations} deserializes multiple responses into one big array.
     *
     * @dataProvider getAllLocationsResponsesProvider
     */
    public function testGetAllLocations(array $deserializedResponses, iterable $expectedResults)
    {
        $this->httpClientMock
            ->expects($this->exactly(count($deserializedResponses)))
            ->method('request')
            ->with('GET', 'https://rickandmortyapi.com/api/location/', $this->anything())
            ->willReturnCallback(function (string $method, string $url) {
                $mockHttpClient = new MockHttpClient([
                    new MockResponse('fake json')
                ]);

                return $mockHttpClient->request($method, $url);
            });

        $this->serializerMock->method('deserialize')
            ->willReturnOnConsecutiveCalls(...$deserializedResponses);

        $allLocations = $this->client->getAllLocations();

        $this->assertEquals($expectedResults, $allLocations);
    }

    /**
     * Data provider for different kinds of successful API responses that should (or not) result in a followup request.
     *
     * @return array
     */
    public function getAllLocationsResponsesProvider()
    {
        $location1 = $this->createLocation(1);
        $location2 = $this->createLocation(2);
        $location3 = $this->createLocation(3);

        return [
            'one-page' => [
                [ // Deserialized values
                    new LocationCollection(
                        new Metadata(1, 1, '', ''),
                        [
                            $location1,
                            $location2,
                        ]
                    ),
                ],
                [ // Expected results
                    $location1,
                    $location2,
                ]
            ],
            'three-pages' => [
                [ // Deserialized values
                    new LocationCollection(
                        new Metadata(3, 3, 'https://rickandmortyapi.com/api/location?page=2', ''),
                        [
                            $location1,
                        ]
                    ),
                    new LocationCollection(
                        new Metadata(3, 3, 'https://rickandmortyapi.com/api/location?page=3', 'https://rickandmortyapi.com/api/location?page=1'),
                        [
                            $location2,
                        ]
                    ),
                    new LocationCollection(
                        new Metadata(3, 3, '', 'https://rickandmortyapi.com/api/location?page=2'),
                        [
                            $location3,
                        ]
                    ),
                ],
                [ // Expected results
                    $location1,
                    $location2,
                    $location3,
                ]
            ],
        ];
    }

    /**
     * Create a character to test with.
     */
    private function createCharacter($id = 1337): Character
    {
        return new Character(
            $id,
            'Unit test character',
            'Not real',
            'Human',
            '',
            'Unknown',
            new LocationLink('', ''),
            new LocationLink('', ''),
            '',
            [],
            '',
            new DateTime()
        );
    }

    /**
     * Create an episode to test with.
     */
    private function createEpisode(int $id = 1337): Episode
    {
        return new Episode(
            $id,
            'Unit test episode',
            new DateTime(),
            'S13E37',
            [],
            '',
            new DateTime()
        );
    }

    /**
     * Create a location to test with.
     */
    private function createLocation(int $id = 1337): Location
    {
        return new Location(
            $id,
            'Unit test location',
            'Not real',
            'Human',
            [],
            'Unknown',
            new DateTime()
        );
    }
}
