<?php

namespace App\Tests\RickAndMortyApi\Normalizer;

use App\RickAndMortyApi\Normalizer\CharacterLinkDenormalizer;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Location;
use Foo;
use PHPUnit\Framework\TestCase;

/**
 * Denormalizer for {@see CharacterLink} objects. This exists because there is an array of URLs in the {@see Location}
 * response that can not be mapped to objects by the serializer component.
 *
 * @package App\RickAndMortyApi\Normalizer
 */
class CharacterLinkDenormalizerTest extends TestCase
{
    /**
     * Class to test.
     *
     * @var CharacterLinkDenormalizer
     */
    private $denormalizer;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->denormalizer = new CharacterLinkDenormalizer();
    }

    /**
     * Test that {@see CharacterLinkDenormalizer::supportsDenormalization} only returns true when the type is correct.
     *
     * @dataProvider typesProvider
     *
     * @param string $type
     * @param bool $expectedResult
     */
    public function testSupportsDenormalization(string $type, bool $expectedResult): void
    {
        $this->assertEquals($expectedResult, $this->denormalizer->supportsDenormalization('', $type));
    }

    /**
     * Data provider that provides different types of objects that might be passed to a serializer.
     */
    public function typesProvider()
    {
        return [
            'The only thing that should return true' => [
                CharacterLink::class,
                true,
            ],
            'A random class' => [
                Foo::class,
                false
            ],
            'A class from the same package' => [
                Character::class,
                false
            ],
        ];
    }

    /**
     * Test that denormalize takes the data and sets it as the url of a new EpisodeLink object.
     */
    public function testDenormalize(): void
    {
        $characterLink = $this->denormalizer->denormalize('http://episode-link-url.com/over-here#anchor', CharacterLink::class);

        $this->assertEquals(new CharacterLink('http://episode-link-url.com/over-here#anchor'), $characterLink);
    }
}
