<?php

namespace App\Tests\RickAndMortyApi\Normalizer;

use App\RickAndMortyApi\Normalizer\EpisodeLinkDenormalizer;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\Link\EpisodeLink;
use Foo;
use PHPUnit\Framework\TestCase;

/**
 * Tests for {@see EpisodeLinkDenormalizer}.
 *
 * @package App\Tests\RickAndMortyApi\Normalizer
 */
class EpisodeLinkDenormalizerTest extends TestCase
{
    /**
     * Class to test.
     *
     * @var EpisodeLinkDenormalizer
     */
    private $denormalizer;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->denormalizer = new EpisodeLinkDenormalizer();
    }

    /**
     * Test that {@see EpisodeLinkDenormalizer::supportsDenormalization} only returns true when the type is correct.
     *
     * @dataProvider typesProvider
     *
     * @param string $type
     * @param bool $expectedResult
     */
    public function testSupportsDenormalization(string $type, bool $expectedResult): void
    {
        $this->assertEquals($expectedResult, $this->denormalizer->supportsDenormalization('', $type));
    }

    /**
     * Data provider that provides different types of objects that might be passed to a serializer.
     */
    public function typesProvider()
    {
        return [
            'The only thing that should return true' => [
                EpisodeLink::class,
                true,
            ],
            'A random class' => [
                Foo::class,
                false
            ],
            'A class from the same package' => [
                Character::class,
                false
            ],
        ];
    }

    /**
     * Test that denormalize takes the data and sets it as the url of a new EpisodeLink object.
     */
    public function testDenormalize(): void
    {
        $episodeLink = $this->denormalizer->denormalize('http://episode-link-url.com/over-here#anchor', EpisodeLink::class);

        $this->assertEquals(new EpisodeLink('http://episode-link-url.com/over-here#anchor'), $episodeLink);
    }
}
