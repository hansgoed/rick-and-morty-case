<?php

namespace App\Request\ParamConverter;

use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Episode;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Convert an id to an {@see Episode}
 *
 * @package App\Request\ParamConverter
 */
class EpisodeParamConverter implements ParamConverterInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * EpisodeParamConverter constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $name = $configuration->getName();

        $id = $request->get($name);

        if (!is_numeric($id)) {
            return false;
        }

        $episode = $this->client->getEpisodeById($id);

        $request->attributes->set($name, $episode);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        if ($configuration->getClass() !== Episode::class) {
            return false;
        }

        return true;
    }
}