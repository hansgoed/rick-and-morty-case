<?php

namespace App\Request\ParamConverter;

use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Character;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Convert an id to an {@see Character}
 *
 * @package App\Request\ParamConverter
 */
class CharacterParamConverter implements ParamConverterInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * EpisodeParamConverter constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $name = $configuration->getName();

        $id = $request->get($name);

        if (!is_numeric($id)) {
            return false;
        }

        $episode = $this->client->getCharacterById($id);

        $request->attributes->set($name, $episode);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        if ($configuration->getClass() !== Character::class) {
            return false;
        }

        return true;
    }
}