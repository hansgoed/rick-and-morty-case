<?php

namespace App\Request\ParamConverter;

use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Location;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Convert an id to an {@see Location}
 *
 * @package App\Request\ParamConverter
 */
class LocationParamConverter implements ParamConverterInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * LocationParamConverter constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $name = $configuration->getName();

        $id = $request->get($name);

        if (!is_numeric($id)) {
            return false;
        }

        $location = $this->client->getLocationById($id);

        $request->attributes->set($name, $location);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        if ($configuration->getClass() !== Location::class) {
            return false;
        }

        return true;
    }
}