<?php

namespace App\Controller;

use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\Link\EpisodeLink;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for every {@see Character} related action.
 *
 * @package App\Controller
 */
class CharacterController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * CharacterController constructor.
     *
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Show a single character.
     *
     * @Route("/character/{character}")
     *
     * @param Character $character
     *
     * @return Response
     */
    public function showCharacter(Character $character, Client $client): Response
    {
        $episodeIds = $this->extractIdsFromEpisodeLinks($character->getEpisode());

        $parameters = [
            'character' => $character,
            'episodes' => $client->getEpisodesById($episodeIds),
        ];

        if ($character->getOrigin()->getUrl() !== '') {
            $parameters['origin'] = $client->getLocationByLink($character->getOrigin());
        }

        if ($character->getLocation()->getUrl() !== '') {
            $parameters['location'] = $client->getLocationByLink($character->getLocation());
        }

        $output = $this->twig->render('character.html.twig', $parameters);

        return new Response($output);
    }

    /**
     * Show all characters.
     *
     * @Route("/")
     * @Route("/characters")
     *
     * @param Client $client
     *
     * @return Response
     */
    public function showCharacters(Client $client): Response
    {
        $characters = $client->getAllCharacters();

        $output = $this->twig->render('characters.html.twig', [
            'characters' => $characters,
        ]);

        return new Response($output);
    }

    /**
     * Get the episode IDs from the URLs in {@see EpisodeLink}s.
     *
     * @param EpisodeLink[] $episodeLinks
     */
    private function extractIdsFromEpisodeLinks(iterable $episodeLinks)
    {
        return array_map(function (EpisodeLink $episode) {
            $url = $episode->getUrl();

            return (int) preg_replace('/\D/', '', $url);
        }, $episodeLinks);
    }
}