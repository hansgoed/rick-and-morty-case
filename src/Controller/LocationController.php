<?php

namespace App\Controller;

use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Location;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for every {@see Location} related action.
 *
 * @package App\Controller
 */
class LocationController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * LocationController constructor.
     *
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Show a single location.
     *
     * @Route("/location/{location}")
     *
     * @param Location $location
     *
     * @return Response
     */
    public function showLocation(Location $location, Client $client): Response
    {
        $output = $this->twig->render('location.html.twig', [
            'location' => $location,
            'residents' => $client->getCharactersByLinks($location->getResidents()),
        ]);

        return new Response($output);
    }

    /**
     * Show all locations.
     *
     * @Route("/locations")
     *
     * @param Client $client
     *
     * @return Response
     */
    public function showLocations(Client $client): Response
    {
        $locations = $client->getAllLocations();

        $output = $this->twig->render('locations.html.twig', [
            'locations' => $locations,
        ]);

        return new Response($output);
    }
}