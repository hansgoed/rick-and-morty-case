<?php

namespace App\Controller;

use App\RickAndMortyApi\Client;
use App\RickAndMortyApi\Response\Episode;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for every {@see Episode} related action.
 *
 * @package App\Controller
 */
class EpisodeController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * EpisodeController constructor.
     *
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Show a single episode.
     *
     * @Route("/episode/{episode}")
     *
     * @param Episode $episode
     *
     * @return Response
     */
    public function showEpisode(Episode $episode, Client $client): Response
    {
        $characters = $client->getCharactersByLinks($episode->getCharacters());

        $output = $this->twig->render('episode.html.twig', [
            'episode' => $episode,
            'characters' => $characters,
        ]);

        return new Response($output);
    }

    /**
     * Show all episodes.
     *
     * @Route("/episodes")
     *
     * @param Client $client
     *
     * @return Response
     */
    public function showEpisodes(Client $client): Response
    {
        $episodes = $client->getAllEpisodes();

        $output = $this->twig->render('episodes.html.twig', [
            'episodes' => $episodes,
        ]);

        return new Response($output);
    }
}