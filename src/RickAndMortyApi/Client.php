<?php

namespace App\RickAndMortyApi;

use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\CharacterCollection;
use App\RickAndMortyApi\Response\Episode;
use App\RickAndMortyApi\Response\EpisodeCollection;
use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Link\EpisodeLink;
use App\RickAndMortyApi\Response\Link\LocationLink;
use App\RickAndMortyApi\Response\Location;
use App\RickAndMortyApi\Response\LocationCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Client for the Rick and Morty REST API.
 *
 * @see https://rickandmortyapi.com/documentation
 *
 * @package App\RickAndMortyApi
 */
class Client
{
    /**
     * Base URI for the Rick and Morty REST API.
     *
     * @var string
     */
    private $baseUri = 'https://rickandmortyapi.com/api';

    /**
     * HttpClient for communication with the REST API.
     *
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * Serializer that can convert the JSON response from the API into response objects.
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * Create a new RickAndMortyApiClient.
     *
     * @param HttpClientInterface $httpClient
     * @param SerializerInterface $serializer
     */
    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer)
    {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * Get a single Rick and Morty character by ID.
     *
     * @param int $characterId
     *
     * @return Character
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getCharacterById(int $characterId): Character
    {
        $url = sprintf(
            '%s/character/%d',
            $this->baseUri,
            $characterId
        );

        return $this->getCharacterByUrl($url);
    }

    /**
     * Get a single Rick and Morty character from a CharacterLink response received from another API call.
     *
     * @param CharacterLink $characterLink
     * @return Character
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getCharacterByLink(CharacterLink $characterLink): Character
    {
        return $this->getCharacterByUrl($characterLink->getUrl());
    }

    /**
     * Get a single Rick and Morty character by endpoint URL.
     *
     * @param string $url
     *
     * @return Character
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getCharacterByUrl(string $url): Character
    {
        $response = $this->httpClient->request(Request::METHOD_GET, $url);
        $characterJSON = $response->getContent();

        /** @var Character $character */
        $character = $this->serializer->deserialize($characterJSON, Character::class, 'json');

        return $character;
    }

    /**
     * Get {@see Character}s matching the filters or all when no filters are given.
     *
     * @param array $filters
     * @param int $page
     *
     * @return CharacterCollection
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getCharacters(array $filters = [], int $page = null): CharacterCollection
    {
        $resolver = $this->getCharactersFiltersResolver();
        $filters = $resolver->resolve($filters);

        $url = sprintf(
            '%s/character/',
            $this->baseUri
        );

        $response = $this->httpClient->request(Request::METHOD_GET, $url, [
            'query' => [
                'name' => $filters['name'],
                'status' => $filters['status'],
                'species' => $filters['species'],
                'type' => $filters['type'],
                'gender' => $filters['gender'],
                'page' => $page,
            ]
        ]);

        $characterCollectionJson = $response->getContent();

        /** @var CharacterCollection $characterCollection */
        $characterCollection = $this->serializer
            ->deserialize($characterCollectionJson, CharacterCollection::class, 'json');

        return $characterCollection;
    }

    /**
     * Get the optionsResolver used to validate the filter input for {@see getCharacters}.
     *
     * @return OptionsResolver
     */
    private function getCharactersFiltersResolver(): OptionsResolver
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults(
            [
                'name' => null,
                'status' => null,
                'species' => null,
                'type' => null,
                'gender' => null
            ]
        );

        $resolver->setAllowedValues(
            'status',
            [
                'alive',
                'dead',
                'unknown',
                null,
            ]
        );

        $resolver->setAllowedValues(
            'gender',
            [
                'female',
                'male',
                'genderless',
                'unknown',
                null,
            ]
        );

        return $resolver;
    }

    /**
     * Get a single Rick and Morty location by ID.
     *
     * @param int $locationId
     *
     * @return Location
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getLocationById(int $locationId): Location
    {
        $url = sprintf(
            '%s/location/%d',
            $this->baseUri,
            $locationId
        );

        return $this->getLocationByUrl($url);
    }

    /**
     * Get a single Rick and Morty location from a LocationLink response received from another API call.
     *
     * @param LocationLink $locationLink
     *
     * @return Location
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getLocationByLink(LocationLink $locationLink): Location
    {
        return $this->getLocationByUrl($locationLink->getUrl());
    }

    /**
     * Get a single Rick and Morty location by endpoint URL.
     *
     * @param string $url
     *
     * @return Location
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getLocationByUrl(string $url): Location
    {
        $response = $this->httpClient->request(Request::METHOD_GET, $url);
        $locationJSON = $response->getContent();

        /** @var Location $location */
        $location = $this->serializer->deserialize($locationJSON, Location::class, 'json');

        return $location;
    }

    /**
     * Get {@see Location}s matching the filters or all when no filters are given.
     *
     * @param array $filters
     * @param int|null $page
     *
     * @return LocationCollection
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getLocations(array $filters = [], int $page = null): LocationCollection
    {
        $resolver = $this->getLocationsFiltersResolver();
        $filters = $resolver->resolve($filters);

        $url = sprintf(
            '%s/location/',
            $this->baseUri
        );

        $response = $this->httpClient->request(Request::METHOD_GET, $url, [
            'query' => [
                'name' => $filters['name'],
                'type' => $filters['type'],
                'dimension' => $filters['dimension'],
                'page' => $page,
            ]
        ]);

        $locationCollectionJson = $response->getContent();

        /** @var LocationCollection $locationCollection */
        $locationCollection = $this->serializer
            ->deserialize($locationCollectionJson, LocationCollection::class, 'json');

        return $locationCollection;
    }

    /**
     * Get the optionsResolver used to validate the filter input for {@see getLocations}.
     *
     * @return OptionsResolver
     */
    private function getLocationsFiltersResolver(): OptionsResolver
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults(
            [
                'name' => null,
                'type' => null,
                'dimension' => null,
            ]
        );

        return $resolver;
    }

    /**
     * Get a single Rick and Morty episode by ID.
     *
     * @param int $episodeId
     *
     * @return Episode
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getEpisodeById(int $episodeId): Episode
    {
        $url = sprintf(
            '%s/episode/%d',
            $this->baseUri,
            $episodeId
        );

        return $this->getEpisodeByUrl($url);
    }

    /**
     * Get a single Rick and Morty episode from a EpisodeLink response received from another API call.
     *
     * @param EpisodeLink $episodeLink
     *
     * @return Episode
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getEpisodeByLink(EpisodeLink $episodeLink): Episode
    {
        return $this->getEpisodeByUrl($episodeLink->getUrl());
    }

    /**
     * Get a single Rick and Morty episode by endpoint URL.
     *
     * @param string $url
     *
     * @return Episode
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getEpisodeByUrl(string $url): Episode
    {
        $response = $this->httpClient->request(Request::METHOD_GET, $url);
        $episodeJSON = $response->getContent();

        /** @var Episode $episode */
        $episode = $this->serializer->deserialize($episodeJSON, Episode::class, 'json');

        return $episode;
    }

    /**
     * Get {@see Episode}s matching the filters or all when no filters are given.
     *
     * @param array $filters
     * @param int $page
     *
     * @return EpisodeCollection
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getEpisodes(array $filters = [], int $page = null): EpisodeCollection
    {
        $resolver = $this->getEpisodesFiltersResolver();
        $filters = $resolver->resolve($filters);

        $url = sprintf(
            '%s/episode/',
            $this->baseUri
        );

        $response = $this->httpClient->request(Request::METHOD_GET, $url, [
            'query' => [
                'name' => $filters['name'],
                'episode' => $filters['episode'],
                'page' => $page,
            ]
        ]);

        $episodeCollectionJson = $response->getContent();

        /** @var EpisodeCollection $episodeCollection */
        $episodeCollection = $this->serializer
            ->deserialize($episodeCollectionJson, EpisodeCollection::class, 'json');

        return $episodeCollection;
    }

    /**
     * Get the optionsResolver used to validate the filter input for {@see getEpisodes}.
     *
     * @return OptionsResolver
     */
    private function getEpisodesFiltersResolver(): OptionsResolver
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults(
            [
                'name' => null,
                'episode' => null,
            ]
        );

        return $resolver;
    }

    /**
     * Fetch multiple episodes by their IDs.
     *
     * @param int[] $episodeIds
     *
     * @return Episode[]
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getEpisodesById(iterable $episodeIds): iterable
    {
        if (count($episodeIds) === 1) {
            return [$this->getEpisodeById($episodeIds[0])];
        }

        $url = sprintf(
            '%s/episode/%s',
            $this->baseUri,
            implode(',', $episodeIds)
        );

        $episodesJson = $this->httpClient->request(Request::METHOD_GET, $url)->getContent();

        /** @var Episode[] $episodes */
        $episodes = $this->serializer
            ->deserialize($episodesJson, Episode::class . '[]', 'json');

        return $episodes;
    }

    /**
     * Get multiple characters by characterLinks.
     *
     * @param CharacterLink[] $characterLinks
     *
     * @return Character[]
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getCharactersByLinks(array $characterLinks): iterable
    {
        if (count($characterLinks) === 1) {
            return [$this->getCharacterByLink($characterLinks[0])];
        }

        $characterIds = $this->extractCharacterIdsFromCharacterLinks($characterLinks);

        $url = sprintf(
            '%s/character/%s',
            $this->baseUri,
            implode(',', $characterIds)
        );

        $episodesJson = $this->httpClient->request(Request::METHOD_GET, $url)->getContent();

        /** @var Character[] $characters */
        $characters = $this->serializer
            ->deserialize($episodesJson, Character::class . '[]', 'json');

        return $characters;
    }

    /**
     * Get the character IDs from the URLs in {@see CharacterLink}s.
     *
     * @param CharacterLink[] $characterLinks
     *
     * @return int[]
     */
    private function extractCharacterIdsFromCharacterLinks(array $characterLinks): iterable
    {
        return array_map(function (CharacterLink $characterLink) {
            // Strip everything but numbers.
            return (int) preg_replace('/\D/', '', $characterLink->getUrl());
        }, $characterLinks);
    }

    /**
     * Fetch all Rick and Morty characters, from all pages.
     *
     * @param Client $client
     *
     * @return Character[]|array
     */
    public function getAllCharacters(): array
    {
        $charactersResponse = $this->getCharacters();
        $characters = $charactersResponse->getResults();

        while (!$charactersResponse->getInfo()->isLastPage()) {
            $charactersResponse = $this->getCharacters(
                [],
                $charactersResponse->getInfo()->getNextPageNumber()
            );

            $characters = array_merge($characters, $charactersResponse->getResults());
        }

        return $characters;
    }

    /**
     * Fetch all Rick and Morty episodes, from all pages.
     *
     * @return array
     */
    public function getAllEpisodes()
    {
        $episodesResponse = $this->getEpisodes();
        $episodes = $episodesResponse->getResults();

        while (!$episodesResponse->getInfo()->isLastPage()) {
            $episodesResponse = $this->getEpisodes(
                [],
                $episodesResponse->getInfo()->getNextPageNumber()
            );

            $episodes = array_merge($episodes, $episodesResponse->getResults());
        }

        return $episodes;
    }

    /**
     * Fetch all Rick and Morty locations, from all pages.
     *
     * @param Client $client
     *
     * @return Location[]|array
     */
    public function getAllLocations(): array
    {
        $locationsResponse = $this->getLocations();
        $locations = $locationsResponse->getResults();

        while (!$locationsResponse->getInfo()->isLastPage()) {
            $locationsResponse = $this->getLocations(
                [],
                $locationsResponse->getInfo()->getNextPageNumber()
            );

            $locations = array_merge($locations, $locationsResponse->getResults());
        }

        return $locations;
    }
}