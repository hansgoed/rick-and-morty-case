<?php

namespace App\RickAndMortyApi\Normalizer;

use App\RickAndMortyApi\Response\Link\CharacterLink;
use App\RickAndMortyApi\Response\Location;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Denormalizer for {@see CharacterLink} objects. This exists because there is an array of URLs in the {@see Location}
 * response that can not be mapped to objects by the serializer component.
 *
 * @package App\RickAndMortyApi\Normalizer
 */
class CharacterLinkDenormalizer implements DenormalizerInterface
{
    /**
     * @inheritDoc
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return new CharacterLink($data);
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, string $type, string $format = null)
    {
        if ($type !== CharacterLink::class) {
            return false;
        }

        return true;
    }
}