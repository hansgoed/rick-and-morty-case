<?php

namespace App\RickAndMortyApi\Normalizer;

use App\RickAndMortyApi\Response\Character;
use App\RickAndMortyApi\Response\Link\EpisodeLink;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Denormalizer for {@see EpisodeLink} objects. This exists because there is an array of URLs in the {@see Character}
 * response that can not be mapped to objects by the serializer component.
 *
 * @package App\RickAndMortyApi\Normalizer
 */
class EpisodeLinkDenormalizer implements DenormalizerInterface
{
    /**
     * @inheritDoc
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return new EpisodeLink($data);
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, string $type, string $format = null)
    {
        if ($type !== EpisodeLink::class) {
            return false;
        }

        return true;
    }
}