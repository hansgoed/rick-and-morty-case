<?php

namespace App\RickAndMortyApi\Response;

/**
 * Representation of a response containing multiple {@see Episode}s and {@see Metadata}.
 *
 * @link https://rickandmortyapi.com/documentation/#get-all-episodes
 * @link https://rickandmortyapi.com/documentation/#filter-episodes
 *
 * @package App\RickAndMortyApi\Response
 */
class EpisodeCollection
{
    /**
     * Metadata for this collection.
     *
     * @var Metadata
     */
    private $info;

    /**
     * Collection of resources of requested type.
     *
     * @var Episode[]
     */
    private $results;

    /**
     * Episode constructor.
     *
     * @param Metadata $info
     * @param Episode[] $results
     */
    public function __construct(Metadata $info, array $results)
    {
        $this->info = $info;
        $this->results = $results;
    }

    /**
     * {@see $info}.
     *
     * @return Metadata
     */
    public function getInfo(): Metadata
    {
        return $this->info;
    }

    /**
     * {@see $results}.
     *
     * @return Episode[]
     */
    public function getResults(): array
    {
        return $this->results;
    }
}