<?php

namespace App\RickAndMortyApi\Response;

/**
 * Representation of a response containing multiple {@see Location}s and {@see Metadata}.
 *
 * @link https://rickandmortyapi.com/documentation/#get-all-locations
 * @link https://rickandmortyapi.com/documentation/#filter-locations
 *
 * @package App\RickAndMortyApi\Response
 */
class LocationCollection
{
    /**
     * Metadata for this collection.
     *
     * @var Metadata
     */
    private $info;

    /**
     * Collection of resources of requested type.
     *
     * @var Location[]
     */
    private $results;

    /**
     * CharacterCollection constructor.
     *
     * @param Metadata $info
     * @param Location[] $results
     */
    public function __construct(Metadata $info, array $results)
    {
        $this->info = $info;
        $this->results = $results;
    }

    /**
     * {@see $info}.
     *
     * @return Metadata
     */
    public function getInfo(): Metadata
    {
        return $this->info;
    }

    /**
     * {@see $results}.
     *
     * @return Location[]
     */
    public function getResults(): array
    {
        return $this->results;
    }
}