<?php

namespace App\RickAndMortyApi\Response;

use App\RickAndMortyApi\Response\Link\CharacterLink;
use DateTimeInterface;

/**
 * A location in the Rick and Morty universe.
 *
 * @package App\RickAndMortyApi\Response
 */
class Location
{
    /**
     * The id of the location.
     *
     * @var int
     */
    private $id;

    /**
     * The name of the location.
     *
     * @var string
     */
    private $name;

    /**
     * The type of the location.
     *
     * @var string
     */
    private $type;

    /**
     * The dimension in which the location is located.
     *
     * @var string
     */
    private $dimension;

    /**
     * List of links to characters who have been last seen in the location.
     *
     * @var CharacterLink[]
     */
    private $residents;

    /**
     * Link to the location's own endpoint.
     *
     * @var string
     */
    private $url;

    /**
     * Time at which the location was created in the database.
     *
     * @var DateTimeInterface
     */
    private $created;

    /**
     * Location constructor.
     *
     * @param int $id
     * @param string $name
     * @param string $type
     * @param string $dimension
     * @param CharacterLink[] $residents
     * @param string $url
     * @param DateTimeInterface $created
     */
    public function __construct(
        int $id,
        string $name,
        string $type,
        string $dimension,
        iterable $residents,
        string $url,
        DateTimeInterface $created
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->dimension = $dimension;
        $this->residents = $residents;
        $this->url = $url;
        $this->created = $created;
    }

    /**
     * {@see $id}.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@see $name}.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@see $type}.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * {@see $dimension}.
     *
     * @return string
     */
    public function getDimension(): string
    {
        return $this->dimension;
    }

    /**
     * {@see $residents}.
     *
     * @return CharacterLink[]
     */
    public function getResidents(): iterable
    {
        return $this->residents;
    }

    /**
     * {@see $url}.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * {@see $created}.
     *
     * @return DateTimeInterface
     */
    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }
}