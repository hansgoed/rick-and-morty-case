<?php

namespace App\RickAndMortyApi\Response;

use LogicException;

/**
 * Representation of "info" that is added to the response for multiple documents.
 *
 * @package App\RickAndMortyApi\Response
 *
 */
class Metadata
{
    /**
     * Total count of the search results.
     *
     * @var int
     */
    private $count;

    /**
     * Total amount of pages.
     *
     * @var int
     */
    private $pages;

    /**
     * URL of the next page.
     *
     * @var string
     */
    private $next;

    /**
     * URL of the previous page.
     *
     * @var string
     */
    private $prev;

    /**
     * Metadata constructor.
     *
     * @param int $count
     * @param int $pages
     * @param string $next
     * @param string $prev
     */
    public function __construct(int $count, int $pages, string $next, string $prev)
    {
        $this->count = $count;
        $this->pages = $pages;
        $this->next = $next;
        $this->prev = $prev;
    }

    /**
     * {@see $count}.
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * {@see $pages}.
     *
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * Are there any pages with results after this one?
     *
     * @return bool
     */
    public function isLastPage(): bool
    {
        if ($this->next !== '') {
            return false;
        }

        return true;
    }

    /**
     * Is this the first page with results?
     *
     * @return bool
     */
    public function isFirstPage() {
        if ($this->prev !== '') {
            return false;
        }

        return true;
    }

    /**
     * Get the number of the page after this one.
     *
     * @return int
     *
     * @throws LogicException when there is no page after this one: Use {@see isLastPage}.
     */
    public function getNextPageNumber(): int
    {
        if ($this->isLastPage()) {
            throw new LogicException("Can not navigate further than the last page.");
        }

        return $this->extractPageNumberFromLink($this->next);
    }

    /**
     * Get the number of the page before this one.
     *
     * @return int
     *
     * @throws LogicException when there is no page before this one: Use {@see isFirstPage}.
     */
    public function getPreviousPageNumber(): int
    {
        if ($this->isFirstPage()) {
            throw new LogicException("Can not navigate further back than the first page.");
        }

        return $this->extractPageNumberFromLink($this->prev);
    }

    /**
     * Get the page number from a link.
     *
     * @param string $link
     * @return int
     */
    private function extractPageNumberFromLink(string $link): int
    {
        $rawQueryString = parse_url($link, PHP_URL_QUERY);
        parse_str($rawQueryString, $queryItems);

        if ($queryItems === null || !isset($queryItems['page'])) {
            $exceptionMessage = sprintf(
                'Page link "%s" doesn\'t have a "page" item in the querystring.',
                $link
            );

            throw new LogicException($exceptionMessage);
        }

        $pageNumber = $queryItems['page'];
        if (!is_numeric($pageNumber)) {
            $exceptionMessage = sprintf(
                'Page link "%s" doesn\'t have a valid "page" item in the querystring.',
                $link
            );

            throw new LogicException($exceptionMessage);
        }

        return (int) $queryItems['page'];
    }
}