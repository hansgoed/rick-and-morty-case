<?php

namespace App\RickAndMortyApi\Response;

/**
 * Representation of a response containing multiple {@see Character}s and {@see Metadata}.
 *
 * @link https://rickandmortyapi.com/documentation/#get-all-characters
 * @link https://rickandmortyapi.com/documentation/#filter-characters
 *
 * @package App\RickAndMortyApi\Response
 */
class CharacterCollection
{
    /**
     * Metadata for this collection
     *
     * @var Metadata
     */
    private $info;

    /**
     * Collection of resources of requested type.
     *
     * @var Character[]
     */
    private $results;

    /**
     * CharacterCollection constructor.
     *
     * @param Metadata $info
     * @param Character[] $results
     */
    public function __construct(Metadata $info, array $results)
    {
        $this->info = $info;
        $this->results = $results;
    }

    /**
     * {@see $info}.
     *
     * @return Metadata
     */
    public function getInfo(): Metadata
    {
        return $this->info;
    }

    /**
     * {@see $results}.
     *
     * @return Character[]
     */
    public function getResults(): array
    {
        return $this->results;
    }
}