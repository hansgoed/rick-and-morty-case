<?php

namespace App\RickAndMortyApi\Response\Link;

use App\RickAndMortyApi\Response\Location;

/**
 * Link to a {@see Location}.
 *
 * @package App\RickAndMortyApi\Response\Link
 */
class LocationLink extends AbstractLink
{
    /**
     * Name of the location.
     *
     * @var string
     */
    private $name;

    /**
     * LocationLink constructor.
     *
     * @param string $name
     * @param string $url
     */
    public function __construct(string $name, string $url)
    {
        parent::__construct($url);

        $this->name = $name;
    }

    /**
     * {@see $name}
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}