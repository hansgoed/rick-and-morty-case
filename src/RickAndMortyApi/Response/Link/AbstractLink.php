<?php

namespace App\RickAndMortyApi\Response\Link;

/**
 * Link to a resource in the API.
 *
 * @package App\RickAndMortyApi\Response\Link
 */
abstract class AbstractLink implements LinkInterface
{
    /**
     * The fully qualified location of the resource.
     *
     * @var string
     */
    protected $url;

    /**
     * AbstractLink constructor.
     *
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * {@see $url}.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}