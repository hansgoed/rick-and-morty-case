<?php

namespace App\RickAndMortyApi\Response\Link;

use App\RickAndMortyApi\Response\Episode;

/**
 * Link to an {@see Episode}.
 *
 * @package App\RickAndMortyApi\Response\Link
 */
class EpisodeLink extends AbstractLink
{
}