<?php

namespace App\RickAndMortyApi\Response\Link;

/**
 * Link to a resource in the API.
 *
 * @package App\RickAndMortyApi\Response\Link
 */
interface LinkInterface
{
    /**
     * The fully qualified location of the resource.
     *
     * @return string
     */
    public function getUrl(): string;
}