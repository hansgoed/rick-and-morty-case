<?php

namespace App\RickAndMortyApi\Response\Link;

use App\RickAndMortyApi\Response\Character;

/**
 * Link to a {@see Character}.
 *
 * @package App\RickAndMortyApi\Response\Link
 */
class CharacterLink extends AbstractLink
{
}