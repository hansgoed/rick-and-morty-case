<?php

namespace App\RickAndMortyApi\Response;

use App\RickAndMortyApi\Response\Link\EpisodeLink;
use App\RickAndMortyApi\Response\Link\LocationLink;
use DateTimeInterface;

/**
 * A character in the Rick and Morty universe.
 */
class Character
{
    /**
     * The id of the character.
     *
     * @var int
     */
    private $id;

    /**
     * The name of the character.
     *
     * @var string
     */
    private $name;

    /**
     * The status of the character ('Alive', 'Dead' or 'unknown').
     *
     * @var string
     */
    private $status;

    /**
     * The species of the character.
     *
     * @var string
     */
    private $species;

    /**
     * The type or subspecies of the character.
     *
     * @var string
     */
    private $type;

    /**
     * The gender of the character ('Female', 'Male', 'Genderless' or 'unknown').
     *
     * @var string
     */
    private $gender;

    /**
     * Link to the character's origin location.
     *
     * @var LocationLink
     */
    private $origin;

    /**
     * Link to the character's last known location.
     *
     * @var LocationLink
     */
    private $location;

    /**
     * URL to the character's image.
     * All images are 300x300px and most are medium shots or portraits since they are intended to be used as avatars.
     *
     * @var string
     */
    private $image;

    /**
     * List of links to episodes in which this character appeared.
     *
     * @var EpisodeLink[]
     */
    private $episode;

    /**
     * URL of the character's own endpoint.
     *
     * @var string
     */
    private $url;

    /**
     * Time at which the character was created in the database.
     *
     * @var DateTimeInterface
     */
    private $created;

    /**
     * Character constructor.
     *
     * @param int $id
     * @param string $name
     * @param string $status
     * @param string $species
     * @param string $type
     * @param string $gender
     * @param LocationLink $origin
     * @param LocationLink $location
     * @param string $image
     * @param EpisodeLink[] $episode
     * @param string $url
     * @param DateTimeInterface $created
     */
    public function __construct(
        int $id,
        string $name,
        string $status,
        string $species,
        string $type,
        string $gender,
        LocationLink $origin,
        LocationLink $location,
        string $image,
        iterable $episode,
        string $url,
        DateTimeInterface $created
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->status = $status;
        $this->species = $species;
        $this->type = $type;
        $this->gender = $gender;
        $this->image = $image;
        $this->url = $url;
        $this->created = $created;
        $this->origin = $origin;
        $this->location = $location;
        $this->episode = $episode;
    }

    /**
     * @return LocationLink
     */
    public function getOrigin(): LocationLink
    {
        return $this->origin;
    }

    /**
     * @return LocationLink
     */
    public function getLocation(): LocationLink
    {
        return $this->location;
    }

    /**
     * @return EpisodeLink[]
     */
    public function getEpisode(): array
    {
        return $this->episode;
    }

    /**
     * @see $id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @see $name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @see $status
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @see $species
     *
     * @return string
     */
    public function getSpecies(): string
    {
        return $this->species;
    }

    /**
     * @see $type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @see $gender
     *
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @see $image
     *
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }
}