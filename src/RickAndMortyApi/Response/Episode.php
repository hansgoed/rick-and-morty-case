<?php

namespace App\RickAndMortyApi\Response;

use App\RickAndMortyApi\Response\Link\CharacterLink;
use DateTimeInterface;

/**
 * A Rick and Morty episode.
 *
 * @package App\RickAndMortyApi\Response
 */
class Episode
{
    /**
     * The id of the episode.
     *
     * @var int
     */
    private $id;

    /**
     * The name of the episode.
     *
     * @var string
     */
    private $name;

    /**
     * The air date of the episode.
     *
     * @var DateTimeInterface
     */
    private $airDate;

    /**
     * The code of the episode.
     *
     * @example S02E10
     *
     * @var string
     */
    private $episode;

    /**
     * List of characters who have been seen in the episode.
     *
     * @var CharacterLink[]
     */
    private $characters;

    /**
     * Link to the episode's own endpoint.
     *
     * @var string
     */
    private $url;

    /**
     * Time at which the episode was created in the database.
     *
     * @var DateTimeInterface
     */
    private $created;

    /**
     * Episode constructor.
     * @param int $id
     * @param string $name
     * @param DateTimeInterface $airDate
     * @param string $episode
     * @param CharacterLink[] $characters
     * @param string $url
     * @param DateTimeInterface $created
     */
    public function __construct(
        int $id,
        string $name,
        DateTimeInterface $airDate,
        string $episode,
        array $characters,
        string $url,
        DateTimeInterface $created
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->airDate = $airDate;
        $this->episode = $episode;
        $this->characters = $characters;
        $this->url = $url;
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return DateTimeInterface
     */
    public function getAirDate(): DateTimeInterface
    {
        return $this->airDate;
    }

    /**
     * @return string
     */
    public function getEpisode(): string
    {
        return $this->episode;
    }

    /**
     * @return CharacterLink[]
     */
    public function getCharacters(): array
    {
        return $this->characters;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }
}